# Using GNU autotools and Rust

In light of the fact that many projects are taking an
interest in using Rust within their existing codebase,
I decided to try and create an up-to-date example to work
from.

There are multiple examples here;

* staticlib using rustc (only) - sub-dir `staticlib-rustc`
* staticlib using cargo (safer, easier) - sub-dir `staticlib-cargo`
* cdylib using cargo - sub-dir `cdylib-cargo`

There is no `cdylib` using `rustc` only as this is troublesome to compile due to having to include the rustc libs in linking.

Run `autoconf -si` to generate the `configure` script, then run `./configure && make`.

The cdylib produces a shared library which will need to be included on the path when running the program eg, `LD_LIBRARY_PATH=. ./hello_rust` when run in the `cdylib-cargo` dir.


One of the many sources I used to figure this out is [from Hubert Figuiere's blog](https://www.figuiere.net/hub/blog/?2016/10/07/862-rust-and-automake).

**NOTE 1:** If you use any external crates, it will be much easier to use `cargo` to build everything.

**NOTE 2:** You will notice that `-C lto` is being passed to the `cargo rustc --release --` command in the `Makefile.am`, this performs LLVM link-time optimzations, and with a staticlib it can reduce the lib size from eg 14mb to 3mb, and dynamic libs from eg 4.4mb to 1.2mb.

**NOTE 3:** Add `-Z print-link-args` to the `cargo rustc --release --` command list to print out the list of args passed to the linker - this will show you why manually linking a `cdylib` using only `rustc` is a little more trouble than it is worth.

## Links

[GNU Autotools](https://www.gnu.org/software/automake/manual/automake.html#Autotools-Introduction)

[Rust](https://www.rust-lang.org/en-US/)

[Handy tips on Autotools](http://www.clearchain.com/blog/posts/autotools)

