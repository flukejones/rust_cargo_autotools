// Don't mangle the names
#[no_mangle]

// extern make function stick to C calling convention
pub extern "C" fn add(a: i32, b: i32) -> i32 {
    a + b
}
