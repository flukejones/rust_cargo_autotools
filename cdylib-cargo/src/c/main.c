#include <stdio.h>
#include <stdint.h>

extern void hello_world();
extern int32_t add(int32_t, int32_t);

int main(void)
{
    hello_world();
    int32_t i = add(20, 22);
    printf("The meaning of life is %d\n", i);
}

int32_t divide(int32_t a, int32_t b)
{
    return a / b;
}
