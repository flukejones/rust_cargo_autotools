extern { pub fn divide(a: i32, b: i32) -> i32; }

// Don't mangle the names
#[no_mangle]
pub extern "C" fn hello_world() {
    println!("hello world!");
    unsafe {
        println!("Divided to {:?} we conquer", divide(42,2));
    }
}
